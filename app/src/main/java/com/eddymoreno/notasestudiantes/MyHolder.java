package com.eddymoreno.notasestudiantes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img;
    TextView nametxt, posTxt, priceTxt;
    ItemClickListener itemClickListener;


    public MyHolder(View itemView) {
        super(itemView);


        nametxt = (TextView) itemView.findViewById(R.id.etOrder);
        posTxt = (TextView) itemView.findViewById(R.id.etQuantity);
        priceTxt = (TextView) itemView.findViewById(R.id.etPrice);
        img = (ImageView) itemView.findViewById(R.id.Image);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic)
    {
        this.itemClickListener = ic;

    }
}
