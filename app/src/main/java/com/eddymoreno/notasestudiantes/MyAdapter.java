package com.eddymoreno.notasestudiantes;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyHolder> {
    Context c;
    ArrayList<Order> orders;

    public MyAdapter(Context c, ArrayList<Order> orders) {
        this.c = c;
        this.orders = orders;
    }

    //INITIALIZE VIEWHOLDER
    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //VIEW OBJ
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model, null);

        //HOLDER
        MyHolder holder = new MyHolder(v);

        return holder;
    }

    //BIND VIEW TO DATA
    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.img.setImageResource(R.drawable.marker);

        holder.nametxt.setText(orders.get(position).getOrder());
        holder.posTxt.setText(orders.get(position).getQuantity());
        holder.priceTxt.setText(orders.get(position).getPrice());

        //CLICKED
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Snackbar.make(v, orders.get(pos).getOrder(), Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
