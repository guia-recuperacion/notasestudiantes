package com.eddymoreno.notasestudiantes;

/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public class Constants {
    //COLUMNS
    static final String ROW_ID = "id";
    static final String ORDER = "order";
    static final String QUANTITY = "quantity";
    static final String PRICE = "price";

    //DB PROPERTIES
    static final String DB_NAME = "d_DB";
    static final String TB_NAME = "d_TB";
    static final int DB_VERSION = '6';

    static final String CREATE_TB = "CREATE TABLE d_TB(id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + " order TEXT NOT NULL, quantity TEXT NOT NULL, price TEXT NOT NULL);";
}
